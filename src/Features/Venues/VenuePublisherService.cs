﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using System.Threading;
using System.Threading.Tasks;

namespace sportsData.Producer.Features.Venues
{
    public class VenuePublisherService : BackgroundService
    {
        private const int DelayInSeconds = 10;
        private const int MillisecondsToSeconds = 1000;

        private readonly ILogger<VenuePublisherService> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public VenuePublisherService(ILogger<VenuePublisherService> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger?.LogInformation("Begin execute");
            while (!cancellationToken.IsCancellationRequested)
            {
                using var venuePublisherScope = _serviceScopeFactory.CreateScope();
                var publisher = venuePublisherScope.ServiceProvider.GetService<VenuePublisher>();
                await publisher.Publish(cancellationToken);
                await Task.Delay(DelayInSeconds * MillisecondsToSeconds, cancellationToken);
            }
        }
    }
}