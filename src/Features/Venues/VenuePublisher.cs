﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using sportsData.Common.Eventing;
using sportsData.Common.ExtensionMethods;
using sportsData.Events.Venue;
using sportsData.Events.Venue.DTOs;
using sportsData.Infrastructure.Espn;
using sportsData.Producer.Data;
using sportsData.Producer.Data.Entities;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using sportsData.Common.Hashing;

namespace sportsData.Producer.Features.Venues
{
    public class VenuePublisher : ResourcePublisherBase
    {
        private readonly ILogger<VenuePublisher> _logger;
        private readonly SportsDataProducerDataContext _dataContext;
        private readonly IProvideEspnApiData _espnApiData;
        private readonly IProvideHashes _hashProvider;

        public VenuePublisher(ILogger<VenuePublisher> logger,
            SportsDataProducerDataContext dataContext,
            IProvideEspnApiData espnApiData,
            IProvideHashes hashProvider) : base((int)EspnResourceType.Venue)
        {
            _logger = logger;
            _dataContext = dataContext;
            _espnApiData = espnApiData;
            _hashProvider = hashProvider;
        }

        public override async Task Publish(CancellationToken cancellationToken)
        {
            var correlationId = Guid.NewGuid();

            // get the list of all venues
            var venues = await _espnApiData.Venues();

            var resourceHash = await _dataContext.ResourceHashes
                .FirstOrDefaultAsync(h => h.TypeId == ResourceTypeId && !h.ResourceId.HasValue, cancellationToken);

            if (resourceHash == null)
            {
                resourceHash = new ResourceHash()
                {
                    CreatedUtc = DateTime.UtcNow,
                    Hash = _hashProvider.GenerateHash(venues.ToJson()),
                    TypeId = ResourceTypeId,
                    LastCheckedUtc = DateTime.UtcNow
                };
                await _dataContext.ResourceHashes.AddAsync(resourceHash, cancellationToken);
                await _dataContext.SaveChangesAsync(cancellationToken);
            }

            var newHash = _hashProvider.GenerateHash(venues.ToJson());
            if (newHash != resourceHash.Hash)
            {
                // update the resourceHash
                resourceHash.Hash = newHash;
                resourceHash.ModifiedUtc = DateTime.UtcNow;

                // TODO: a venue has been added; find it
            }
            else
            {
                resourceHash.LastCheckedUtc = DateTime.UtcNow;
            }

            // now check all Venues for updates
            await venues.items.ForEachAsync(async i =>
            {
                var venue = await _espnApiData.Venue(i.id, true);
                await ProcessVenueAsync(venue, cancellationToken, correlationId);
            });
        }

        private async Task ProcessVenueAsync(sportsData.Infrastructure.Espn.Dtos.Venue.Venue venue,
            CancellationToken cancellationToken,
            Guid correlationId)
        {
            var venueMd5 = _hashProvider.GenerateHash(venue.ToJson());

            var venueHash = await _dataContext.ResourceHashes
                .FirstOrDefaultAsync(h => h.TypeId == ResourceTypeId && h.ResourceId == venue.Id, cancellationToken);

            var causationId = Guid.NewGuid();

            string eventType;
            string eventPayload;

            if (venueHash == null)
            {
                // new Venue
                venueHash = new ResourceHash()
                {
                    CreatedUtc = DateTime.UtcNow,
                    Hash = _hashProvider.GenerateHash(venue.ToJson()),
                    TypeId = ResourceTypeId,
                    ResourceId = venue.Id,
                    SourceId = 1
                };
                await _dataContext.ResourceHashes.AddAsync(venueHash, cancellationToken);
                var venueCreatedEvent = MapCreated(venue, correlationId, causationId);
                eventPayload = venueCreatedEvent.ToJson();
                eventType = nameof(VenueCreatedEvent);
            }
            else
            {
                if (venueHash.Hash == venueMd5)
                {
                    // no change
                    venueHash.LastCheckedUtc = DateTime.UtcNow;
                    await _dataContext.SaveChangesAsync(cancellationToken);
                    return;
                }

                // updated Venue
                var venueUpdatedEvent = MapUpdated(venue, correlationId, causationId);
                eventPayload = venueUpdatedEvent.ToJson();
                eventType = nameof(VenueUpdatedEvent);
            }

            var outgoingEvent = new OutgoingEvent()
            {
                CorrelationId = correlationId,
                CausationId = causationId,
                CreatedBy = 99,
                CreatedUtc = DateTime.UtcNow,
                EventPayload = eventPayload,
                EventType = eventType,
                Id = Guid.NewGuid()
            };

            await _dataContext.OutgoingEvents.AddAsync(outgoingEvent, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);
        }

        private static VenueCreatedEvent MapCreated(sportsData.Infrastructure.Espn.Dtos.Venue.Venue espnVenue, Guid correlationId, Guid causationId)
        {
            return new VenueCreatedEvent(correlationId, causationId)
            {
                CausationId = causationId,
                CorrelationId = correlationId,
                CreatedBy = 99,
                CreatedByName = nameof(VenuePublisher),
                CreatedUtc = DateTime.UtcNow,
                Venue = Map(espnVenue)
            };
        }

        private static VenueUpdatedEvent MapUpdated(sportsData.Infrastructure.Espn.Dtos.Venue.Venue espnVenue, Guid correlationId, Guid causationId)
        {
            return new VenueUpdatedEvent(correlationId, causationId)
            {
                CausationId = causationId,
                CorrelationId = correlationId,
                CreatedBy = 99,
                CreatedByName = nameof(VenuePublisher),
                CreatedUtc = DateTime.UtcNow,
                Venue = Map(espnVenue)
            };
        }

        private static Venue Map(sportsData.Infrastructure.Espn.Dtos.Venue.Venue espnVenue)
        {
            return new Venue()
            {
                Address = Map(espnVenue.Address),
                Capacity = espnVenue.Capacity,
                FullName = espnVenue.FullName,
                Id = espnVenue.Id.ToString(),
                Images = espnVenue.Images?.Select(Map).ToList(),
                IsGrass = espnVenue.Grass,
                IsIndoor = espnVenue.Indoor,
                ShortName = espnVenue.ShortName
            };
        }

        private static VenueAddress Map(sportsData.Infrastructure.Espn.Dtos.Venue.Address espnVenueAddress)
        {
            return new VenueAddress()
            {
                City = espnVenueAddress.City,
                State = espnVenueAddress.State,
                Zip = espnVenueAddress.ZipCode
            };
        }

        private static VenueImage Map(sportsData.Infrastructure.Espn.Dtos.Venue.Image espnVenueImage)
        {
            return new VenueImage()
            {
                Alt = espnVenueImage.Alt,
                Height = espnVenueImage.Height,
                Href = espnVenueImage.Href,
                Rel = espnVenueImage.Rel,
                Width = espnVenueImage.Width
            };
        }
    }
}