﻿using Microsoft.Extensions.Hosting;

using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace sportsData.Producer.Features
{

    public interface IPublishResources
    {
        Task Publish(CancellationToken cancellationToken);
    }

    public abstract class ResourcePublisherBase : IPublishResources
    {
        public int ResourceTypeId { get; set; }

        protected ResourcePublisherBase(int resourceTypeId)
        {
            ResourceTypeId = resourceTypeId;
        }

        public abstract Task Publish(CancellationToken cancellationToken);
    }
}