﻿using Microsoft.EntityFrameworkCore;

using sportsData.Common.Eventing;
using sportsData.Common.Eventing.Providers;
using sportsData.Producer.Data.Entities;

namespace sportsData.Producer.Data
{
    public class SportsDataProducerDataContext : DbContext, IProvideEventingData
    {
        public SportsDataProducerDataContext(DbContextOptions<SportsDataProducerDataContext> options)
            : base(options) { }

        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        public DbSet<IncomingEvent> IncomingEvents { get; set; }
        public DbSet<ResourceHash> ResourceHashes { get; set; }
    }
}