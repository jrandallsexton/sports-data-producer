﻿
namespace sportsData.Producer.Data.Entities
{
    public class Resource
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}