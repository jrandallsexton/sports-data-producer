﻿using System;

namespace sportsData.Producer.Data.Entities
{
    public class ResourceHash
    {
        /// <summary>
        /// PK of the record
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id of the source; i.e. ESPN, CBS-sports, etc.
        /// </summary>
        public int SourceId { get; set; }

        /// <summary>
        /// Type of resource; i.e. Venue, Franchise, Team, etc.
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Specific id of the resource; i.e.VenueId, FranchiseId, TeamId, etc.
        /// </summary>
        public long? ResourceId { get; set; }

        /// <summary>
        /// MD5 hash of the previous json response for this resource
        /// </summary>
        public string Hash { get; set; }

        /// <summary>
        /// When this MD5 was created in UTC
        /// </summary>
        public DateTime CreatedUtc { get; set; }

        /// <summary>
        /// Last time this MD5 was updated in UTC
        /// </summary>
        public DateTime? ModifiedUtc { get; set; }

        public DateTime LastCheckedUtc { get; set; }
    }
}