﻿using FluentAssertions;

using Microsoft.EntityFrameworkCore;

using Moq.AutoMock;

using sportsData.Common.Eventing;
using sportsData.Common.Eventing.Providers;
using sportsData.Events;
using sportsData.Producer.Data;

using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace sportsData.Producer.Tests
{
    public abstract class UnitTestBase<T>
    {

        public int ValidUserId = 99;

        public static SportsDataProducerDataContext GetDataContext()
        {
            return new SportsDataProducerDataContext(GetDataContextOptions<SportsDataProducerDataContext>());
        }

        public static EventingDataContext GetEventingDataContext()
        {
            return new EventingDataContext(GetDataContextOptions<EventingDataContext>());
        }

        public static DbContextOptions<TDc> GetDataContextOptions<TDc>() where TDc : DbContext
        {
            // https://stackoverflow.com/questions/52810039/moq-and-setting-up-db-context
            var dbName = Guid.NewGuid().ToString().Substring(0, 5);
            return new DbContextOptionsBuilder<TDc>()
                .UseInMemoryDatabase(dbName)
                .Options;
        }

        public async Task<AutoMocker> GetDefaultMocker()
        {
            return await Task.Run(() =>
            {
                var mocker = new AutoMocker();
                return mocker;
            });
        }

        public async Task AssertCreatedEventCount<TE>(IProvideEventingData dataContact, int expectedCount) where TE : EventBase
        {
            var eventCount = await GetCreatedEventCount<TE>(dataContact);
            eventCount.Should().Be(expectedCount);
        }

        private static async Task<int> GetCreatedEventCount<TE>(IProvideEventingData dataContext) where TE : EventBase
        {
            var outboxMessages = await dataContext.OutgoingEvents
                .AsNoTracking()
                .Where(m => m.EventType == typeof(TE).Name)
                .ToListAsync();

            return outboxMessages?.Count ?? 0;
        }

        public async Task AssertReceivedEventCount<TE>(IProvideEventingData dataContact, int expectedCount) where TE : EventBase
        {
            var eventCount = await GetReceivedEventCount<TE>(dataContact);
            eventCount.Should().Be(expectedCount);
        }

        private static async Task<int> GetReceivedEventCount<TE>(IProvideEventingData dataContext) where TE : EventBase
        {
            var outboxMessages = await dataContext.IncomingEvents
                .AsNoTracking()
                .Where(m => m.EventType == typeof(TE).Name)
                .ToListAsync();

            return outboxMessages?.Count ?? 0;
        }

        public static string CreateMd5(string input)
        {
            // Source:  https://stackoverflow.com/questions/11454004/calculate-a-md5-hash-from-a-string

            // Use input string to calculate MD5 hash
            using var md5 = System.Security.Cryptography.MD5.Create();
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            var hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            var sb = new StringBuilder();
            for (var i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(i.ToString("X2"));
            }
            return sb.ToString();
        }
    }

    public class EventingDataContext : DbContext, IProvideEventingData
    {
        public EventingDataContext(DbContextOptions<EventingDataContext> options)
            : base(options) { }

        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        public DbSet<IncomingEvent> IncomingEvents { get; set; }

        public async Task<int> SaveChanges(CancellationToken cancellationToken = default)
        {
            return await base.SaveChangesAsync(cancellationToken);
        }
        public new void Dispose()
        {
            base.Dispose();
        }
    }
}
