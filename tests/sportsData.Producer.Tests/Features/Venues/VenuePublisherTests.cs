﻿using FluentAssertions;

using Moq;

using sportsData.Common.ExtensionMethods;
using sportsData.Common.Hashing;
using sportsData.Events.Venue;
using sportsData.Infrastructure;
using sportsData.Infrastructure.Espn;
using sportsData.Producer.Data;
using sportsData.Producer.Data.Entities;
using sportsData.Producer.Features.Venues;
using sportsData.Producer.Tests.Mocks;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Xunit;

namespace sportsData.Producer.Tests.Features.Venues
{
    public class VenuePublisherTests : UnitTestBase<VenuePublisher>
    {
        [Fact]
        public async Task OnPublish_NoExistingVenues_CreatesResourceHashes()
        {
            var mocker = await GetDefaultMocker();

            // Arrange
            var espnApiDataMock = new EspnApiDataMock();
            mocker.Use(typeof(IProvideEspnApiData), espnApiDataMock);

            // ... we actually need real hashing here
            mocker.Use(typeof(IProvideHashes), new Md5HashProvider());

            await using var context = GetDataContext();
            mocker.Use(typeof(SportsDataProducerDataContext), context);

            var publisher = mocker.CreateInstance<VenuePublisher>();

            // Act
            await publisher.Publish(CancellationToken.None);

            // Assert
            context.ResourceHashes.Count().Should().Be(2); // 1 for venues, 1 for venue
            await AssertCreatedEventCount<VenueCreatedEvent>(context, 1);
        }

        [Fact]
        public async Task OnPublish_HasExistingVenue_VenueUpdated_UpdatesResourceHash_RaisesUpdatedEvent()
        {
            var mocker = await GetDefaultMocker();

            // Arrange
            var espnDataMocker = new EspnApiDataMock();
            var venues = await espnDataMocker.Venues();
            var venue = await espnDataMocker.Venue(1, true);

            var espnApiDataMock = mocker.GetMock<IProvideEspnApiData>();
            espnApiDataMock.Setup(s => s.Venues())
                .ReturnsAsync(() => venues);
            espnApiDataMock.Setup(s => s.Venue(It.IsAny<int>(), It.IsAny<bool>()))
                .ReturnsAsync(() => venue);

            // ... we actually need real hashing here
            mocker.Use(typeof(IProvideHashes), new Md5HashProvider());

            await using var context = GetDataContext();
            mocker.Use(typeof(SportsDataProducerDataContext), context);

            // ... add the venues hash
            var venuesResourceHash = new ResourceHash()
            {
                CreatedUtc = DateTime.UtcNow,
                Hash = CreateMd5(venues.ToJson()),
                SourceId = (int)ResourceSource.Espn,
                TypeId = (int)EspnResourceType.Venue
            };
            await context.ResourceHashes.AddAsync(venuesResourceHash, CancellationToken.None);

            // ... add the venue hash
            var venueResourceHash = new ResourceHash()
            {
                CreatedUtc = DateTime.UtcNow,
                Hash = CreateMd5(venue.ToJson()),
                SourceId = (int) ResourceSource.Espn,
                TypeId = (int) EspnResourceType.Venue,
                ResourceId = venue.Id
            };
            await context.ResourceHashes.AddAsync(venueResourceHash, CancellationToken.None);

            // ... save both resource hashes
            await context.SaveChangesAsync(CancellationToken.None);

            var publisher = mocker.CreateInstance<VenuePublisher>();

            venue.Capacity += 1; /* SUT */

            // Act
            await publisher.Publish(CancellationToken.None);

            // Assert
            context.ResourceHashes.Count().Should().Be(2); // 1 for venues, 1 for venue
            await AssertCreatedEventCount<VenueUpdatedEvent>(context, 1);
        }
    }
}