﻿using sportsData.Infrastructure.Espn;
using sportsData.Infrastructure.Espn.Dtos.Award;
using sportsData.Infrastructure.Espn.Dtos.Franchise;
using sportsData.Infrastructure.Espn.Dtos.TeamInformation;
using sportsData.Infrastructure.Espn.ResourceIndex;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Item = sportsData.Infrastructure.Espn.ResourceIndex.Item;
using Team = sportsData.Infrastructure.Espn.Dtos.Team.Team;
using Venue = sportsData.Infrastructure.Espn.Dtos.Venue.Venue;

namespace sportsData.Producer.Tests.Mocks
{
    public class EspnApiDataMock : IProvideEspnApiData
    {
        public async Task<ResourceIndex> Awards(int franchiseId)
        {
            await Task.Delay(100);
            throw new NotImplementedException();
        }

        public async Task<List<Award>> AwardsByFranchise(int franchiseId)
        {
            await Task.Delay(100);
            throw new NotImplementedException();
        }

        public async Task<Franchise> Franchise(int franchiseId)
        {
            await Task.Delay(100);
            throw new NotImplementedException();
        }

        public async Task<ResourceIndex> Franchises()
        {
            await Task.Delay(100);
            throw new NotImplementedException();
        }

        public async Task<Team> EspnTeam(int fourDigitYear, int teamId)
        {
            await Task.Delay(100);
            throw new NotImplementedException();
        }

        public async Task<TeamInformation> TeamInformation(int teamId)
        {
            await Task.Delay(100);
            throw new NotImplementedException();
        }

        public async Task<ResourceIndex> Teams(int fourDigitYear)
        {
            await Task.Delay(100);
            throw new NotImplementedException();
        }

        public async Task<ResourceIndex> Venues()
        {
            return await Task.Run(() => new ResourceIndex()
            {
                count = 1,
                pageCount = 1,
                pageIndex = 1,
                pageSize = 1,
                items = new List<Item>()
                {
                    new Item()
                    {
                        id = 1,
                        href = "href-1"
                    }
                }
            });
        }

        public async Task<Venue> Venue(int venueId, bool ignoreCache)
        {
            return await Task.Run(() => new Venue()
            {
                Id = venueId,
                Capacity = 99999,
                FullName = "fullName-value",
                Grass = true,
                Indoor = false,
                Ref = new Uri("http://foo.com"),
                ShortName = "shortName-value",
                Address = new sportsData.Infrastructure.Espn.Dtos.Venue.Address()
                {
                    City = "city-value",
                    State = "state-value",
                    ZipCode = 33990
                },
                Images = new List<sportsData.Infrastructure.Espn.Dtos.Venue.Image>()
                {
                    new sportsData.Infrastructure.Espn.Dtos.Venue.Image()
                    {
                        Alt = "alt-value",
                        Height = 480,
                        Href = new Uri("http://foo.com/img.jpg"),
                        Width = 680,
                        Rel = new List<string>() { "inside", "full-view" }
                    }
                }
            });
        }

        public async Task<byte[]> GetMedia(string uri)
        {
            await Task.Delay(100);
            throw new NotImplementedException();
        }
    }
}